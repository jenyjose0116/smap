.. SMAP documentation master file, created by
   sphinx-quickstart on Wed Aug  5 13:28:17 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _SMAPquickstartindex:

Installation & Quick Start
==========================

| This quickstart guide is for installing and testing various modules of the SMAP package.  
| Install the SMAP package according to these :ref:`installation instructions <SMAPinstallationquickstart>`.
| Test data may be downloaded from Zenodo (link).  

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   quickstart

