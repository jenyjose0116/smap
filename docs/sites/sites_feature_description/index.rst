.. SMAP documentation master file, created by
   sphinx-quickstart on Wed Aug  5 13:28:17 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
.. raw:: html

    <style> .purple {color:purple} </style>

.. role:: purple


Feature description
===================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   LibraryPrep/index
   SampleType/index