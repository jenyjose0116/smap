
.. raw:: html

    <style> .purple {color:purple} </style>

.. role:: purple

.. raw:: html

    <style> .white {color:white} </style>

.. role:: white

############
How It Works
############

.. _SMAPdesignHIW:

Workflow of SMAP design
+++++++++++++++++++++++

| The target genes must be provided as a FASTA and associated GFF file containing at least the *gene*, *CDS* and *exon* features, which can be obtained using **SMAP selection**.
|
| The gRNAs are provided by the user as a list with the standard output of CRISPOR, FlashFry, or any other tab-delimited file with matching format. The list is filtered based on several criteria: gRNAs with a poly-T stretch (≥4T; a Pol III termination signal), and *Bsa*\I and *Bbs*\I restriction sites (for cloning) are removed and only gRNAs targeting the ‘central part’ of the CDS (as defined by the user as a length percentage) are retained. The gRNAs must also have a minimum user-defined MIT score, when available. For each gene, Primer3 designs a maximum of 150 amplicons (by default) of a user-defined size range. The specificity of each primer is tested against all reference sequences, ensuring no cross-amplification is possible. Amplicons are spaced by a minimum of 5 bp to spread the amplicons across the target genes.
|
| Filtered gRNAs are overlapped with the Primer3-generated amplicons. The amplicons are then ranked based on the gRNAs they cover: first on the number of the gRNAs (an amplicon with two gRNAs will rank higher than an amplicon with one gRNA), second whether the gRNAs overlap with each other (amplicons with non-overlapping gRNAs will rank highest), third on the average gRNA specificity scores, and fourth on the average gRNA efficiency scores. If no specificity or efficiency scores are provided in the gRNA file, amplicons are only ranked based on the number of gRNAs and their relative positions. **SMAP design** selects a (user-defined) maximum number of non-overlapping top-ranking amplicons per gene, each covering a (user-defined) maximum number of gRNAs.  
|
| **SMAP design** generates three output files: a tab-separated values (TSV) file with the primer sequences per gene, a TSV file with the gRNA sequences per gene, and a GFF3 file with the primer and gRNA location on the reference sequences (and other annotation features that were included in the GFF file provided by the user). Sequences for which no design was retained are included at the end of the primer and gRNA file with extra information on the reason for design failure. Optionally, **SMAP design** creates a summary file and summary graphs, to facilitate quick evaluation of the set of gRNAs and amplicons. These graphs show the relationship between the number of gRNAs and the number of non-overlapping amplicons per gene that **SMAP design** generated and indicate the reasons for not retaining any gRNA-overlapping amplicons on given genes. This is either because no gRNAs were designed for that gene, none of the gRNAs passed the filters, Primer3 was not capable of designing specific amplicons for the gene, or there was no overlap between the gRNAs and the amplicons.


SMAP target-selection
+++++++++++++++++++++

The **SMAP utility** tool :ref:`SMAP target-selection <SMAP_target-selection_usage>` is run prior to **SMAP design**.  

**SMAP design** minimally requires as input a FASTA file with target sequences and a GFF file with gene features such as gene, CDS, exon.
Once the FASTA and GFF files are obtained, **SMAP design** is run with these files and optionally with a gRNA file. **SMAP design** first filters the gRNAs from the list and generates amplicons on the reference sequences.

----

gRNA filtering
++++++++++++++

| gRNAs are designed by third-party software like :ref:`FlashFry or CRISPOR <SMAPDesigngRNA>`.
| **SMAP design** applies a couple of filters to gRNAs:

* First, **SMAP design** checks for each gRNA sequence whether it is indeed present in the reference sequence FASTA file and to which strand it corresponds.
* Next, gRNAs with poly-T stretches are discarded since they create a termination signal for Pol III.
* gRNAs with *Bsa*\I or *Bbs*\I recognition sites are also discarded since those restriction enzymes are very often used to clone the gRNAs into expression vectors. To find these sites, the gRNA sequence (without PAM) is extended by the last 6 bases of the promoter and first 6 bases of the scaffold as these extensions can create additional restriction sites.
* gRNAs with an MIT score (also known as Hsu score) below the threshold are discarded. The MIT score gives an indication on the specificity of the gRNA. The higher the MIT score the more specific the gRNA. More info on the MIT score can be found `here <https://pubmed.ncbi.nlm.nih.gov/23873081/>`_.
* gRNAs that target the upstream or downstream ends of the CDS are discarded by default. A gRNA targetting the start of the CDS has a chance of creating an alternative translational start site which can result in a slightly truncated, yet functional protein. A gRNA targeting the end of the CDS might not result in a full knock-out. **SMAP design** calculates the length of the CDS and the position of the gRNA in the CDS; if the gRNA targets the first or last 20% of the CDS length (by default), the gRNA is discarded. As such, the length of the introns do not influence the calculation. Users can adjust the length of 5' and 3' excluded CDS regions.
* The output of FlashFry or CRISPOR can be used directly as input of **SMAP design**. The first row of the gRNA file should be a header and is skipped.

Amplicon generation
+++++++++++++++++++

Primer3 is used to generate amplicons on each target gene with the following parameters::

    'PRIMER_PRODUCT_SIZE_RANGE': [[-minl, -maxl]],
    'PRIMER_NUM_RETURN': --generateAmplicons,
    'PRIMER_MAX_LIBRARY_MISPRIMING': --primerMaxLibraryMispriming,
    'PRIMER_PAIR_MAX_LIBRARY_MISPRIMING': --primerPairMaxLibraryMispriming,
    'PRIMER_MAX_TEMPLATE_MISPRIMING': --primerMaxTemplateMispriming,
    'PRIMER_PAIR_MAX_TEMPALTE_MISPRIMING': --primerPairMaxTemplateMispriming,
    'PRIMER_MIN_LEFT_THREE_PRIME_DISTANCE': 5,
    'PRIMER_MIN_RIGHT_THREE_PRIME_DISTANCE': 5,

* The **PRIMER_PRODUCT_SIZE_RANGE** parameter determines the size range of the amplicons. The default is set to 120 - 150 bp
* The **PRIMER_NUM_RETURN** parameter  determines the maximum number of amplicons that Primer3 should generate per reference sequence. The default is set to 150 amplicons.
* The **PRIMER_MAX_LIBRARY_MISPRIMING** parameter is the maximum score of a primer to be retained. The score is based on the ability of the primer to bind to other reference sequences in the FASTA file. The default is set to 12.
* The **PRIMER_PAIR_MAX_LIBRARY_MISPRIMING** parameter is the maximum score of a primer pair to be retained. The score is based on the ability of the primer to bind to other reference sequences in the FASTA file. The default is set to 24.
* The **PRIMER_MAX_TEMPLATE_MISPRIMING** parameter is the maximum score of a primer to be retained. The score is based on the ability of the primer to bind elsewhere in the reference sequence.
* The **PRIMER_PAIR_MAX_TEMPLATE_MISPRIMING** parameter is the maximum score a primer pair can have to be used. The score is based on the ability of the primer to bind elsewhere in the reference sequence.
* The **PRIMER_MIN_LEFT_THREE_PRIME_DISTANCE** parameter determines the minimum number of bases between the ends of the left primers. This is set to 5 bp to prevent design of amplicons around hotspots and so spread the amplicons across the reference sequence.
* The **PRIMER_MIN_RIGHT_THREE_PRIME_DISTANCE** parameter determines the minimum number of bases between the ends of the right primers. This is set to 5 bp to prevent design of amplicons around hotspots and so spread the amplicons across the reference sequence.

A mispriming library is given to Primer3 consisting of all reference sequences in the FASTA file. This will ensure that no primers can bind to other reference sequences. These sets of reference sequences can conveniently be created with **SMAP target-selection**.

Assignment of gRNAs to amplicons
++++++++++++++++++++++++++++++++

If a gRNA is located between the coordinates of the forward and reverse primer and there is a minimum distance (by default 15 bp) between the gRNA binding site (includnig the PAM) and both primers, the gRNA is retained. gRNAs are assigned to the amplicons in order of highest specificity and efficiency scores, until the maximum allowed number of assigned gRNAs per amplicon is reached ``--numbergRNAs``.

Amplicon ranking
++++++++++++++++

| At this stage, the amplicons are ranked according to the gRNAs that were assigned to the amplicon.

* First the amplicons are ranked based on the number of gRNAs that were assigned. If the user set the ``--numbergRNAs`` parameter to 3, amplicons with 3 amplicons will be ranked first, followed by amplicons with 2 gRNAs and then amplicons with 1 gRNA.
* Next, within the groups of amplicons with an equal number of gRNAs, the amplicons for which the gRNAs do not overlap are ranked above the amplicons for which the gRNAs do overlap. This is to spread the gRNA target sites as much as possible within each amplicon.
* Then, the average MIT score (specificity score) and average number of off-targets of the gRNAs per amplicon is calculated. The amplicons with the highest average MIT score and the lowest number of off-targets are ranked highest.
* Finally, the average Doench score (efficiency score) and average OOF score of the gRNAs per amplicon is calculated. The amplicons with the highest average Doench and OOF score are ranked highest.

Amplicon and gRNA selection
+++++++++++++++++++++++++++

| To pick the best scoring amplicons, the position in the gene of the highest ranking amplicon is compared to the position of the second highest ranking amplicon.
| If the amplicons do not overlap, the two amplicons are retained. If the amplicons overlap, the position of the highest ranking amplicon is compared to the position of the third highest ranking amplicon and checked for overlap and so on until the maximum number of allowed non-overlapping amplicons per gene is reached.
| If the maximum number of non-overlapping amplicons is not reached, the amplicon combination with the most amplicons is selected.
| The information (ID, position, sequences...) of the selected amplicons and gRNAs are output to primer, gRNA, and GFF files.
